//
// Created by varun on 11/5/18.
//

#include <jni.h>
#include <string>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;

extern "C" {
    jint Java_com_netvirtachallenge_camera2_ReactCamera2View_detectCircle(
            JNIEnv *, jobject, jlong addGray) {

        Mat& mat  = *(Mat*)addGray;

        cvtColor(mat, mat, COLOR_YUV2BGR_I420);
        cvtColor(mat, mat, COLOR_BGR2GRAY);
        GaussianBlur(mat, mat, Size(9, 9), 2, 2);

        Mat circles;
        // accumulator value
        double dp = 1.2;
        // minimum distance between the center coordinates of detected circles in pixels
        double minDist = 1;
        // min and max radii (set these values as you desire)
        int minRadius = 0, maxRadius = 1000  ;

        double param1 = 70, param2 = 72;

        HoughCircles(mat, circles,
                     CV_HOUGH_GRADIENT, dp, minDist, param1,
                     param2, minRadius, maxRadius);

        int numberOfCircles = (circles.rows == 0) ? 0 : circles.rows;
        return numberOfCircles;
    }

}