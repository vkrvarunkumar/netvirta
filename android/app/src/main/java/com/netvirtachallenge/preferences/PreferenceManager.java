package com.netvirtachallenge.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.netvirtachallenge.constants.Constants;

//Preference manager for managing shared preferences

public class PreferenceManager {

    private static SharedPreferences lensPref;

    public static void init(Context context) {
        lensPref = context.getSharedPreferences(Constants.LENS_CHOICE, Context.MODE_PRIVATE);
    }

    public static void setLensPref(Integer loggedOut) {
        lensPref.edit().putInt(Constants.LENS_CHOICE, loggedOut).apply();
    }

    public static Integer getLensPref() {
        return lensPref.getInt(Constants.LENS_CHOICE,1);
    }
}

