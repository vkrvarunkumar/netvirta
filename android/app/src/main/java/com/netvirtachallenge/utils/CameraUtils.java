package com.netvirtachallenge.utils;

import android.content.Context;
import android.graphics.Matrix;
import android.hardware.camera2.CameraManager;
import android.util.Log;
import android.util.Size;
import android.view.TextureView;

import com.facebook.react.uimanager.ThemedReactContext;

import java.util.Comparator;

/*
    Util class
 */
public class CameraUtils {

    private static final String TAG = CameraUtils.class.getSimpleName();
    private ThemedReactContext context;
    public CameraUtils(ThemedReactContext context) {
        this.context = context;
    }

    /*
        This function scales the preview so that it fits the screen and maintains aspect ratio
     */
    public void applyTransformForPreview(float screenWidth,
                                         float screenHeight, Size mPreviewSize, TextureView textureView) {


        /*
            On setting aspect ratio of TextureView to screenSize the image will get stretched,
            these values find the factor by which they stretch
         */
        float stretchX = (screenHeight / (float) mPreviewSize.getHeight());
        float stretchY = (screenWidth / (float) mPreviewSize.getWidth());

        float factor;

        /*
            We scale image by a factor which depends on whether the the height will be cropped or width
            If height difference between screen and preview is more than the corresponding width difference
            we scale so that the height fits the screen. Else we do the opposite
         */
        if (screenHeight - mPreviewSize.getHeight() > screenWidth - mPreviewSize.getWidth()) {
            factor = screenHeight / (float) mPreviewSize.getHeight();
        } else {
            factor = screenWidth / (float) mPreviewSize.getWidth();
        }

        // Finally we set transform using the initial stretch and the factor calculated and use the
        // pivot as the top center of the screen
        Matrix matrix = new Matrix();
        matrix.setScale(factor / stretchX, factor / stretchY, screenHeight / 2, 0);
        textureView.setTransform(matrix);
    }

    //Fetching the camera manager
    public CameraManager getCameraManager() {
        CameraManager cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        if (cameraManager == null) {
            Log.e(TAG, "Failed to retrieve camera manager");
        }
        return cameraManager;
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    public class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

}
