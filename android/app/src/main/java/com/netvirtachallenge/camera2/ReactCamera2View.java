package com.netvirtachallenge.camera2;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.react.uimanager.ThemedReactContext;
import com.netvirtachallenge.R;
import com.netvirtachallenge.preferences.PreferenceManager;
import com.netvirtachallenge.utils.CameraUtils;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@SuppressLint("ViewConstructor")
public class ReactCamera2View extends RelativeLayout {

    private static final String TAG = ReactCamera2View.class.getSimpleName();
    // Max preview width that is guaranteed by Camera2 API in landscape
    private static final int MAX_PREVIEW_WIDTH = 1920;
    // Max preview height that is guaranteed by Camera2 API in landscape
    private static final int MAX_PREVIEW_HEIGHT = 1080;
    private Toast toast;
    private HandlerThread mBackgroundThread;
    private AutoFitTextureView textureView;
    private Handler backgroundHandler;

    private ThemedReactContext context;
    //Image available listener that will receive the images from the preview
    //and send to OpenCV after converting them to openCV Mat format
    ImageReader.OnImageAvailableListener imageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Image img = reader.acquireLatestImage();
            if (img != null) {
                findCircles(createCVMatFromImage(img));
                img.close();
            } else {
                Log.e(TAG, "Image object acquired was null");
            }
        }

        //Creating OpenCV Mat from Image
        private Mat createCVMatFromImage(Image img) {
            ByteBuffer buffer = img.getPlanes()[0].getBuffer();
            final byte[] data = new byte[buffer.limit()];
            buffer.get(data);

            Mat imageMat = new Mat(img.getHeight() + img.getHeight() / 2, img.getWidth(), CvType.CV_8UC1);
            imageMat.put(0, 0, data);
            return imageMat;
        }

        //Find circles by feeding the Mat into OpenCV
        private void findCircles(Mat mat) {
            showVisualCue(detectCircle(mat.getNativeObjAddr()));
        }

        //Visual cue, toast, is shown if the number of circles > 0
        private void showVisualCue(int numberOfCircles) {
            if (numberOfCircles > 0) {
                toast = Toast.makeText(context, R.string.circle_found, Toast.LENGTH_SHORT);
                toast.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toast.cancel();
                    }
                }, 5);
            }
        }
    };
    private CameraUtils cameraUtils;
    private String cameraId;
    private CameraCaptureSession cameraCaptureSession;
    private CameraDevice cameraDevice;
    private CaptureRequest.Builder previewRequestBuilder;
    private CaptureRequest captureRequest;

    private ImageReader imageReader;
    private Size mPreviewSize;
    private boolean mFlashSupported;

    public ReactCamera2View(ThemedReactContext context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        cameraUtils = new CameraUtils(context);
        inflateLayout(context);
    }

    //View is inflated after it is initialized
    private void inflateLayout(ThemedReactContext context) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.main, this);

        //TetureView, with its underlying SurfaceTexture, is used for displaying content on the screen, in this case our live preview
        textureView = view.findViewById(R.id.texture);
    }

    //Called when activity resumes
    public void onResume() {
        //Starting the background thread used for handling the camera2 api sessions & callbacks
        startBackgroundThread();
        //Opening camera as per the status of texture view
        checkTextureViewAndOpenCamera();
    }

    private void checkTextureViewAndOpenCamera() {
        /*
         When the screen is turned off and turned back on, the SurfaceTexture is already
         available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
         a camera and start preview from here (otherwise, we wait until the surface is ready in
         the SurfaceTextureListener).
        */
        if (textureView.isAvailable()) {
            openCamera(textureView.getWidth(), textureView.getHeight());
        } else {
            textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                    Log.d(TAG, "Texture available");
                    openCamera(width, height);
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                    return true;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

                }
            });
        }
    }

    //Closing camera and stopping background thread when activity pauses
    public void onPause() {
        Log.d(TAG, "onPause()");
        closeCamera();
        stopBackgroundThread();
    }

    private void closeCamera() {
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }

        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    /* Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        backgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            backgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Setting up camera
    private void setUpCamera(int textureViewWidth, int textureViewHeight) {
        CameraManager cameraManager = cameraUtils.getCameraManager();
        if (cameraManager == null) {
            return;
        }

        try {

            //We find the list of connected camera devices and use the one we want
            for (String cameraId : cameraManager.getCameraIdList()) {

                //We get the characteristics of the camera device
                CameraCharacteristics cameraCharacteristics =
                        cameraManager.getCameraCharacteristics(cameraId);

                //We find the value of the LENS_FACING parameter
                Integer facing = cameraCharacteristics.get(CameraCharacteristics.LENS_FACING);

                //We chose the lens according to user preference, default being the back camera lens
                if (facing != null && facing.equals(PreferenceManager.getLensPref())) {
                    StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(
                            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                    if (streamConfigurationMap != null) {
                        this.cameraId = cameraId;
                        //We use the smallest available size which will be used
                        //as the size of the images the image reader will produce
                        Size imageReaderImageSize = Collections.min(Arrays.asList(streamConfigurationMap.getOutputSizes(ImageFormat.JPEG)),
                                cameraUtils.new CompareSizesByArea());
                        //ImageReader created that will be responsible for sending the images to OpenCV for processing
                        //maxImages is set to 4 to prevent surface from being unavailable at any time
                        imageReader = ImageReader.newInstance(imageReaderImageSize.getWidth(), imageReaderImageSize.getHeight(),
                                ImageFormat.YUV_420_888, 4);
                        imageReader.setOnImageAvailableListener(imageAvailableListener, backgroundHandler);

                        // For still image captures, we use the largest available size.
                        Size largest = Collections.max(
                                Arrays.asList(streamConfigurationMap.getOutputSizes(ImageFormat.JPEG)),
                                cameraUtils.new CompareSizesByArea());

                        //Getting screen height and width
                        Point displaySize = new Point();
                        context.getCurrentActivity().getWindowManager().getDefaultDisplay().getRealSize(displaySize);

                        int maxPreviewWidth = displaySize.y;
                        int maxPreviewHeight = displaySize.x;

                        //Finding maxPreview height and width according to screen size and max supported by camera2 api
                        if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                            maxPreviewWidth = MAX_PREVIEW_WIDTH;
                        }

                        if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                            maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                        }

                        //Choosing optimal preview size for efficient and decent looking preview
                        mPreviewSize = chooseOptimalSize(streamConfigurationMap.getOutputSizes(SurfaceTexture.class),
                                textureViewWidth, textureViewHeight, maxPreviewWidth,
                                maxPreviewHeight, largest);

                        createFullScreenPreview(displaySize.y, displaySize.x);

                        // Check if the flash is supported.
                        Boolean available = cameraCharacteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                        mFlashSupported = available == null ? false : available;
                    }
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    /**
     * Given choices of Sizes supported by a camera, we choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                   int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, cameraUtils.new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, cameraUtils.new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    //Ensures that the preview is full screen and preserves aspect ratio
    private void createFullScreenPreview(int screenWidth, int screenHeight) {
        // We fit the aspect ratio of TextureView to the screen size.
        textureView.setAspectRatio(
                screenWidth, screenHeight);

        //We scale the preview so as to fit the screen and not be stretched
        cameraUtils.applyTransformForPreview(screenWidth, screenHeight,
                mPreviewSize, textureView);
    }

    /**
     * Tries to open a {@link CameraDevice}. The result is listened by `mStateCallback`.
     */
    private void openCamera(int textureViewWidth, int textureViewHeight) {
        setUpCamera(textureViewWidth, textureViewHeight);
        //Once the camera is set up, we need to open it
        try {
            CameraManager cameraManager = cameraUtils.getCameraManager();
            if (cameraManager == null) {
                return;
            }
            //Checking for permissions again, in the case when permissions were revoked while app was in background
            if (ContextCompat.checkSelfPermission(context.getCurrentActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            //Opening camera by passing the cameraId, callback and background thread
            cameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice cameraDevice) {
                    ReactCamera2View.this.cameraDevice = cameraDevice;
                    //Creating the preview session when camera opens
                    createPreviewSession();
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                    //Close the camera when it is disconnected
                    cameraDevice.close();
                    ReactCamera2View.this.cameraDevice = null;
                }

                @Override
                public void onError(@NonNull CameraDevice cameraDevice, int error) {
                    Log.i(TAG, "Camera error " + error);
                    //Close the camera when some error occurs
                    cameraDevice.close();
                    ReactCamera2View.this.cameraDevice = null;
                }
            }, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
            Log.i(TAG, e.getMessage());
        }
    }

    private void createPreviewSession() {
        try {

            SurfaceTexture surfaceTexture = textureView.getSurfaceTexture();
            // We configure the size of default buffer to be the size of camera preview we want.
            surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);

            //Request for live preview is created and the previewSurface is
            //set as the target so that we can see the preview on the screen
            previewRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            previewRequestBuilder.addTarget(previewSurface);

            //The imageReader surface is also added as a target so that we can get images
            previewRequestBuilder.addTarget(imageReader.getSurface());

            //Capture session is created and the targets, callback and handler supplied
            cameraDevice.createCaptureSession(Arrays.asList(previewSurface, imageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            if (cameraDevice == null) {
                                return;
                            }

                            try {
                                ReactCamera2View.this.cameraCaptureSession = cameraCaptureSession;

                                // Auto focus should be continuous for camera preview.
                                previewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                // Flash is automatically enabled when necessary.
                                setAutoFlash(previewRequestBuilder);

                                captureRequest = previewRequestBuilder.build();
                                //We start camera preview. We use setRepeatingRequests to continuously
                                //capture images as we need a live preview instead of a still image capture
                                ReactCamera2View.this.cameraCaptureSession.setRepeatingRequest(captureRequest,
                                        new CameraCaptureSession.CaptureCallback() {

                                            @Override
                                            public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                                                           @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                                                super.onCaptureCompleted(session, request, result);
                                            }
                                        }, backgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                        }
                    }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setAutoFlash(CaptureRequest.Builder requestBuilder) {
        if (mFlashSupported) {
            requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                    CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
        }
    }

    //Method for switching between front and back camera,
    //preferences are stored in shared preferences
    public void switchCamera() {
        Integer desiredCamera = PreferenceManager.getLensPref();
        if (desiredCamera.equals(CameraCharacteristics.LENS_FACING_FRONT)) {
            PreferenceManager.setLensPref(CameraCharacteristics.LENS_FACING_BACK);
            restartCamera();
        } else if (desiredCamera.equals(CameraCharacteristics.LENS_FACING_BACK)) {
            PreferenceManager.setLensPref(CameraCharacteristics.LENS_FACING_FRONT);
            restartCamera();
        }
    }

    private void restartCamera() {
        closeCamera();
        checkTextureViewAndOpenCamera();
    }

    public native int detectCircle(long mat);
}
