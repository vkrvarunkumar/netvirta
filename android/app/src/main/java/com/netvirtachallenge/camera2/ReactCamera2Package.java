package com.netvirtachallenge.camera2;


//Created by react-native-create-bridge

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.Collections;
import java.util.List;

public class ReactCamera2Package implements ReactPackage {

    //We don't have any modules so we register nothing
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        // Register your native module
        // https://facebook.github.io/react-native/docs/native-modules-android.html#register-the-module
        return Collections.emptyList();
    }


    //We register the ReactCamera2Manager to the application so that it is available from JavaScript
    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        // Register your native component's view manager
        // https://facebook.github.io/react-native/docs/native-components-android.html#4-register-the-viewmanager
        return Collections.<ViewManager>singletonList(
                new ReactCamera2Manager()
        );
    }
}
