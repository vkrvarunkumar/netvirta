package com.netvirtachallenge.camera2;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.WindowManager;

import com.facebook.infer.annotation.Assertions;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;

import java.util.Map;

/*
    The view manager class ReactCamera2Manager extends SimpleViewManager of type ReactCamera2View.
    ReactCamera2View is the type of object managed by the manager, this will be the custom native view.
*/
public class ReactCamera2Manager extends SimpleViewManager<ReactCamera2View> {

    private static final String REACT_CLASS = "Camera2";
    private static final String TAG = ReactCamera2Manager.class.getSimpleName();
    private static final int CAMERA_REQUEST_CODE = 5;
    private static final int COMMAND_SWITCH_CAMERA = 1;


    //This name is used to reference the native view from JavaScript
    @Override
    public String getName() {
        return REACT_CLASS;
    }

    //Used to create the view instances
    @Override
    public ReactCamera2View createViewInstance(ThemedReactContext context) {

        final ReactCamera2View view = new ReactCamera2View(context, null);

        //This listener is used to listen to lifecycle events.
        //Camera states are changed as per the changes in the activity state
        final LifecycleEventListener listener = new LifecycleEventListener() {
            @Override
            public void onHostResume() {
                Log.d(TAG, "onHostResume");
                view.onResume();
            }

            @Override
            public void onHostPause() {
                Log.d(TAG, "onHostPause");
                view.onPause();
            }

            @Override
            public void onHostDestroy() {
                Log.d(TAG, "onHostDestroy");
            }
        };

        context.addLifecycleEventListener(listener);

        //We are checking camera permission here. If app doesn't have permission, we ask it by showing a dialog box
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context.getCurrentActivity(), new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
        }

        context.getCurrentActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        return view;
    }

    @Override
    public Map<String, Integer> getCommandsMap() {
        Log.d("React", " View manager getCommandsMap:");
        return MapBuilder.of(
                "switchCamera",
                COMMAND_SWITCH_CAMERA);
    }

    //Receive command is used to switch camera here, from the javascript code.
    //It is a way for the Javascript code to communicate with the native code
    @Override
    public void receiveCommand(
            ReactCamera2View view,
            int commandType,
            @Nullable ReadableArray args) {
        Assertions.assertNotNull(view);
        Assertions.assertNotNull(args);
        switch (commandType) {
            case COMMAND_SWITCH_CAMERA: {
                view.switchCamera();
                return;
            }

            default:
                throw new IllegalArgumentException(String.format(
                        "Unsupported command %d received by %s.",
                        commandType,
                        getClass().getSimpleName()));
        }
    }
}
