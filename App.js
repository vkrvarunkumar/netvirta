import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Button,
} from 'react-native';

import Camera2View from './Camera2Component/Camera2NativeView'

export default class NativeView extends Component {

  //renders a button for changing camera and the Camera2View Component

  render() {
    return (
          <View style={{flex:1}}> 
            <Button
              style={{flex:1}}
              onPress={this.switchCamera.bind(this)}
              title="Switch camera"
              color="#841584"
            />
            <Camera2View
              style={{flex:9}}
              ref={ el => {this.cam = el}}/>
          </View>

    );
  }

  switchCamera = () =>{
    this.cam.switchCamera();        
  }
}


AppRegistry.registerComponent('NativeView', () => NativeView);