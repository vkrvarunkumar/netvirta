//  Created by react-native-create-bridge

import React, { Component } from 'react'
import { requireNativeComponent, UIManager, findNodeHandle } from 'react-native'

const Camera2 = requireNativeComponent('Camera2', Camera2View)

//Component that acts as an interface between the Java Native View and Javascript code
export default class Camera2View extends Component {
  switchCamera() {
    UIManager.dispatchViewManagerCommand(
        findNodeHandle(this),
        UIManager.Camera2.Commands.switchCamera,
        [],
    );
  }

  render () {
    return <Camera2 switchCamera={this.switchCamera} {...this.props}/>
  }
}